﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CongNgheWeb_3_0.Models;
using PagedList;
using PagedList.Mvc;
using Common;

namespace CongNgheWeb_3_0.Controllers
{
    public class HomeController : Controller
    {
        E_ClassEntities1 db = new E_ClassEntities1();
        public ActionResult Index()
        {
           //free course
            var ListFree = db.tbl_Course.Where(x=>x.censored==true && x.Pirce==null).Take(5).ToList();
            ViewBag.ListCourse = ListFree;

            ////newest course
            var ListNew = db.tbl_Course.Where(x => x.censored == true).OrderByDescending(x => x.CreateDate).Take(5);

            //List Slide
            var ListSilde = db.tbl_Slide.Where(x => x.Clock == false).OrderByDescending(x=>x.CreateDate).Take(2).ToList();
            ViewBag.ListSilde = ListSilde;

            //Menu Parent
            var MenuParent = db.tbl_Menu.Where(x => x.ParentID == null).ToList();
            ViewBag.MenuParent = MenuParent;

            //Menu child
            var MenuChild = db.tbl_Menu.Where(x => x.ParentID == null).ToList();
            ViewBag.MenuParent = MenuParent;

            //_PartialLikest
            var Likest = db.tbl_Course.Where(x => x.censored == true && x.Pirce == null).ToList();
            ViewBag.ListCourse = ListFree;

            ViewBag.ListNew = ListNew;

            //Count Teacher
            ViewBag.CountTeacher = db.tbl_User.Where(x => x.Position == 3).Count();

            var FirstSlide = db.tbl_Slide.FirstOrDefault(x => x.Clock == false).SildeImage;
            ViewBag.FirstSlide = FirstSlide;

            //Course sale off
            var CourseSaleOff = db.tbl_Course.Where(x => x.Discount != null && x.censored == true).OrderByDescending(x => x.CreateDate).Take(2).ToList();
            ViewBag.CourseSaleOff = CourseSaleOff;

            //show Teacher
            var Teacher = db.tbl_User.Where(x => x.Position ==3 && x.Clock == false).OrderByDescending(x => x.tbl_Course.Count()).Take(3).ToList();
            ViewBag.Teacher = Teacher;
            return View();
        }

        public ActionResult _PartialFreeCourse()
        {
            
            return View();
        }






        public ActionResult _PartialNewCourse()
        {
            return View();
        }

        public ActionResult _PartialLikest()
        {
            return View();
        }
        public ActionResult _PartialTeacher()
        {
            return View();
        }
        //All course of teacher
        public ActionResult AllCourseOfTeacher(int id)
        {
            var Teacher = db.tbl_User.Find(id);
            if(Teacher==null)
            {
                return View("Shared", "Error");
            }
            ViewBag.Teacher = Teacher.UserName;
            return View(db.tbl_Course.Where(x=>x.UserID==id && x.censored==true).OrderByDescending(x=>x.CreateDate).ToList());
        }
        //All course of Sale off
        public ActionResult AllCourseOfSaleOff()
        {
            
            return View(db.tbl_Course.Where(x => x.Discount==null && x.Discount!=0 && x.censored == true).OrderByDescending(x => x.CreateDate).ToList());
        }

        //All course of free
        public ActionResult AllCourseOfFree()
        {
           
            return View(db.tbl_Course.Where(x => x.Pirce==null && x.censored == true).OrderByDescending(x => x.CreateDate).ToList());
        }
        //All course of newest
        public ActionResult AllCourseOfNew()
        {
            return View(db.tbl_Course.Where(x => x.censored == true).OrderByDescending(x => x.CreateDate).ToList());
        }
        //Course of Ngoại ngữ
        public ActionResult AllCourseOfNgoaiNgu()
        {
            return View(db.tbl_Course.Where(x => x.tbl_Menu.ParentID==1002).OrderByDescending(x => x.CreateDate).ToList());
        }
        //Course of Lập trình
        public ActionResult AllCourseOfLapTrinh()
        {
            return View(db.tbl_Course.Where(x => x.tbl_Menu.ParentID == 1).OrderByDescending(x => x.CreateDate).ToList());
        }
        //Course of Kỹ năng sống
        public ActionResult AllCourseOfKyNangSong()
        {
            return View(db.tbl_Course.Where(x => x.tbl_Menu.ParentID == 2).OrderByDescending(x => x.CreateDate).ToList());
        }
        //Course of Nuôi dạy con
        public ActionResult AllCourseOfNuoiDayCon()
        {
            return View(db.tbl_Course.Where(x => x.tbl_Menu.ParentID == 3).OrderByDescending(x => x.CreateDate).ToList());
        }
        //Course of Kinh doanh
        public ActionResult AllCourseOfKinhDoanh()
        {
            return View(db.tbl_Course.Where(x => x.tbl_Menu.ParentID == 1005).OrderByDescending(x => x.CreateDate).ToList());
        }
        //Course Detail
        public ActionResult DetailCourse(int id,int userId)
        {
            var Course = db.tbl_Course.SingleOrDefault(x => x.ID == id);
            if (Course == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            int hasCourse = 0;
            if (userId == 0)
            {
                hasCourse = 0;
            }
            else
            {
                List<tbl_Deal> listDealOfStudent = db.tbl_Deal.Where(x => x.UserID == userId).ToList();
                foreach (var item in listDealOfStudent)
                {
                    if (item.CourseID == id)
                    {
                        hasCourse = 1;
                    }
                }
            }
            ViewBag.Comment = db.tbl_Comment.Where(x => x.CourseID == id).ToList();
            ViewBag.HasCourse = hasCourse;


            return View(Course);
        }
        //Course Detail get from deal
        public ActionResult DetailCourseDeal(int id,string ms)
        {
            var Course = db.tbl_Course.SingleOrDefault(x => x.ID == id);
            if (Course == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            ViewBag.Status = ms;
            return View(Course);
        }
        //Register student
        [HttpGet]
        public ActionResult Register()
        {
            var CourseNew = db.tbl_Course.Where(x => x.censored == true).OrderByDescending(x => x.CreateDate).Take(5);
            ViewBag.CourseNew = CourseNew;
            return View();
        }
        //string email,string Password, string ConfirmPassword,string UseName, string NumberPhone
        [HttpPost]
        public ActionResult Register(RegisterModel Register)
        {
            var CourseNew = db.tbl_Course.Where(x => x.censored == true).OrderByDescending(x => x.CreateDate).Take(5);
            ViewBag.CourseNew = CourseNew;
            if (ModelState.IsValid)
            {
                //if(ConfirmPassword==null)
                //{
                //    ViewBag.Status = "Nhập xác nhận mật khẩu";
                //    return View();
                //}
                //if(string.Compare(user.PasswordUser,ConfirmPassword)==1)
                //{
                //    ViewBag.Status = "Xác nhận mật khẩu không đúng";
                //    return View();
                //}
                var ConfirmEmail = db.tbl_User.Where(x=>x.Email==Register.Email).SingleOrDefault();
                if(ConfirmEmail != null)
                {
                    ViewBag.Status = "Email đã được đăng ký";
                    return View();
                }
                var ConfirmNumberPhone = db.tbl_User.Where(x => x.NumberPhone == Register.NumberPhone).SingleOrDefault();
                if (ConfirmNumberPhone != null)
                {
                    ViewBag.Status = "Số điện thoại này đã tồn tại - yêu cầu nhập số điện thoại khác";
                    return View();
                }
                tbl_User User = new tbl_User();
                User.Clock = false;
                User.CreateDate = DateTime.Now;
                User.DescriptionUser = null;
                User.Email = Register.Email;
                User.LoginName= Register.Email;
                User.Notificationed = false;
                User.NumberPhone = Register.NumberPhone;
                User.PasswordUser = HashPassword.Hash(Register.Password);
                User.Position = 4;
                User.SendMail = true;
                User.UserImage = "no-image.jpg";
                User.UserName = Register.UserName;

                db.tbl_User.Add(User);
                db.SaveChanges();

                return RedirectToAction("Login", "Home");

            }
            ViewBag.Status = "Không tạo được tài khoản";
            return View();
        }

        //Login
        [HttpGet]
        public ActionResult Login()
        {
            var CourseNew = db.tbl_Course.Where(x => x.censored == true).OrderByDescending(x => x.CreateDate).Take(5);
            ViewBag.CourseNew = CourseNew;
            return View();
        }
        //string email,string Password, string ConfirmPassword,string UseName, string NumberPhone
        [HttpPost]
        public ActionResult Login(LoginModel Login)
        {
            var CourseNew = db.tbl_Course.Where(x => x.censored == true).OrderByDescending(x => x.CreateDate).Take(5);
            ViewBag.CourseNew = CourseNew;
            //if(UserName==null)
            //{
            //    ViewBag.UserName = "Tên đăng nhập";
            //    return View();
            //}
            //if (Password == null)
            //{
            //    ViewBag.Password = "Mật khẩu";
            //    return View();
            //}
            if (ModelState.IsValid)
            {
                var User = db.tbl_User.Where(x => x.Email == Login.UserName).SingleOrDefault();
                if (User == null)
                {
                    ViewBag.Status = "Email hoặc mật khẩu không đúng";
                    return View();
                }
                if (HashPassword.UnHash(Login.Password, User.PasswordUser) == true)
                {
                    Session["Position"] = User.Position;
                    Session["UserName"] = User.UserName;
                    Session["ID"] = User.ID;
                    Session["UserImage"] = User.UserImage;
                    if (User.Position == 4)//student
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    if(User.Position == 3)//teacher
                    {
                        return RedirectToAction("Index", "GiangVien");
                    }
                    if (User.Position == 1)//teacher
                    {
                        return RedirectToAction("Index", "Admin");
                    }

                }
                //string UnHashPassword=Common.HashPassword.UnHash(Password.User)


               
            }
            //ViewBag.Status = "Email hoặc mật khẩu không đúng";
            return View();

        }
        public ActionResult Logout()
        {
            Session.RemoveAll();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult ShowSlide()
        {
            var FirstSlide = db.tbl_Slide.FirstOrDefault(x => x.Clock == false).SildeImage;
            ViewBag.FirstSlide = FirstSlide;

            var ListSilde = db.tbl_Slide.Where(x => x.Clock == false).OrderByDescending(x => x.CreateDate).Take(2).ToList();
            ViewBag.ListSilde = ListSilde;

            //Course sale off
            var CourseSaleOff = db.tbl_Course.Where(x => x.Discount != null && x.censored == true).OrderByDescending(x => x.CreateDate).Take(2).ToList();
            ViewBag.CourseSaleOff = CourseSaleOff;
            return View(ListSilde);
        }
        //Show menu
        public PartialViewResult _PartialMenuParent()
        {
            return PartialView(db.tbl_Menu.Where(x => x.ParentID == null).ToList());
        }

        [ChildActionOnly]
        public PartialViewResult MenuChild(int ParentID)
        {
            return PartialView(db.tbl_Menu.Where(x => x.ParentID == ParentID).ToList());
        }

        //Course by menu id
        public ActionResult CourseByMenuID(int id)
        {
            var Menu= db.tbl_Menu.Find(id);
            ViewBag.CourseName = Menu.MenuName;
            ViewBag.CourseIcon = Menu.Icon;
            ViewBag.CourseCount = db.tbl_Course.Where(x => x.MenuID == id && x.censored==true).Count();
            return View(db.tbl_Course.Where(x => x.MenuID == id && x.censored==true).ToList());
        }
        //Reset password
        [HttpPost]
        public ActionResult ResetPassword(string Email)
        {

            return View();
        }
        //change of user
        //id is id of user
        [HttpGet]
        public ActionResult ChangePassword(int id)
        {
            var user = db.tbl_User.Find(id);
            if (user == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            return View(user);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ChangePassword(tbl_User User, string OldPassword, string NewPassword, string ConfirmNewPassword)
        {
            if (HashPassword.UnHash(OldPassword, User.PasswordUser) == false)
            {
                ViewBag.Status = "Mật khẩu không đúng - thử lại!";
                return View();
            }
            else if (NewPassword != ConfirmNewPassword)
            {
                ViewBag.Status = "Mật khẩu xác nhận không đúng - thử lại!";
                return View();
            }
            User.PasswordUser = HashPassword.Hash(NewPassword);
            db.Entry(User).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            Session.RemoveAll();
            return RedirectToAction("Index", "Home");
            //ViewBag.Status = "Cập nhật thông tin thành công";
            //return View();
        }
        //Infor of teacher
        public ActionResult InforTeacher(int id)
        {
            var Teacher = db.tbl_User.Find(id);
            ViewBag.Teacher = Teacher;

            var Course = db.tbl_Course.Where(x => x.UserID == id && x.censored == true).Take(4).ToList();
            ViewBag.Course = Course;

            return View();
        }
    }
}