﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CongNgheWeb_3_0.Models;

namespace CongNgheWeb_3_0.Controllers
{
    public class AdminController : BaseAdminController
    {
        E_ClassEntities1 db = new E_ClassEntities1();
        // GET: Admin
        public ActionResult Index()
        {
            ViewBag.CountStudent = db.tbl_User.Where(x => x.Position == 4).Count();
            ViewBag.CountTeacher = db.tbl_User.Where(x => x.Position == 3).Count();
            ViewBag.CountCourse = db.tbl_Course.Count();
            ViewBag.ListKhoaHocGiangVienGuiLen = db.tbl_Course.Where(x => x.censored == false && x.Sended == true).Count();
            

            string dateNow = DateTime.Now.ToString("dd/MM/yyyy");
            //----Tổng số học viên đăng ký hôm nay-----------------
            int HocVienDangKyTKHomNay = 0;
            List<tbl_User> listUser = db.tbl_User.ToList();
            foreach(var item in listUser)
            {
                if(item.CreateDate.Value.ToString("dd/MM/yyyy") == dateNow)
                {
                    HocVienDangKyTKHomNay++;
                }
            }
            ViewBag.HocVienDangKyTKHomNay = HocVienDangKyTKHomNay;
            //----------------------------------------------------

            //----Tổng học viên có đăng ký khóa học hôm nay-------
            List<tbl_Deal> listKhoaHocDuocDangKyHomNay = new List<tbl_Deal>();
            int HocVienDangKyKHHomNay = 0;
            List<tbl_Deal> listDeal = db.tbl_Deal.ToList();
            foreach (var item in listDeal)
            {
                if (item.CreateDate.Value.ToString("dd/MM/yyyy") == dateNow)
                {
                    HocVienDangKyKHHomNay++;
                    listKhoaHocDuocDangKyHomNay.Add(item);
                }
            }
            ViewBag.HocVienDangKyKHHomNay = HocVienDangKyKHHomNay;
            //----------------------------------------------------

            //----Tổng số khóa học được đăng ký hôm nay-----------
            
            ViewBag.HocVienDangKyKHHomNay = listKhoaHocDuocDangKyHomNay.GroupBy(x => x.CourseID).Count();
            //----------------------------------------------------
            ViewBag.KhoaHocDuocDangKyHomNay = db.tbl_Deal.Where(x => x.CreateDate == DateTime.Now).GroupBy(x=>x.CourseID).Count();

            //----Đếm bình luận hôm nay
            List<tbl_Comment> listBinhLuanHomNay = new List<tbl_Comment>();
            List<tbl_Comment> listAllBinhLuan = db.tbl_Comment.ToList();
            foreach (var item in listAllBinhLuan)
            {
                if (item.CreateDay.Value.ToString("dd/MM/yyyy") == dateNow)
                {
                    listBinhLuanHomNay.Add(item);
                }
            }
            ViewBag.BinhLuanHomNay = listBinhLuanHomNay.Count();
            return View();
        }

        //Get: list teacher
        public ActionResult ListTeacher()
        {
            var ListTeacher = db.tbl_User.Where(x=>x.Position==3).OrderBy(x=>x.UserName).ToList();
            return View(ListTeacher);
        }
        public ActionResult ListChuaDuyet()
        {
            List<tbl_Course> listChuaDuyet = db.tbl_Course.Where(x => x.censored==false && x.Sended==true).OrderBy(x => x.CreateDate).ToList();

            return View(listChuaDuyet);
        }
        public ActionResult BinhLuanHomNay()
        {
            string dateNow = DateTime.Now.ToString("dd/MM/yyyy");
            List<tbl_Comment> listBinhLuanHomNay = new List<tbl_Comment>();
            List<tbl_Comment> listAllBinhLuan = db.tbl_Comment.ToList();
            foreach(var item in listAllBinhLuan)
            {
                if(item.CreateDay.Value.ToString("dd/MM/yyyy")== dateNow)
                {
                    listBinhLuanHomNay.Add(item);
                }
            }


            return View(listBinhLuanHomNay);
        }
        public ActionResult DeleteComment(int id)
        {
            tbl_Comment comment = db.tbl_Comment.SingleOrDefault(x => x.ID == id);
            if (comment == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            return View(comment);
        }
        public ActionResult ConfirmDeleteComment(int id)
        {
            tbl_Comment comment = db.tbl_Comment.SingleOrDefault(x => x.ID == id);
            if (comment == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            db.tbl_Comment.Remove(comment);
            db.SaveChanges();
            return RedirectToAction("BinhLuanHomNay", "Admin");
        }
        public ActionResult KhoaHocDuocDangKyHomNay()
        {
            List<tbl_Deal> listKhoaHocDuocDangKyHomNay = db.tbl_Deal.Where(x => x.CreateDate==DateTime.Now).ToList();

            return View(listKhoaHocDuocDangKyHomNay);
        }
    }
}