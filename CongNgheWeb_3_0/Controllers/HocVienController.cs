﻿using CongNgheWeb_3_0.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CongNgheWeb_3_0.Controllers
{
    public class HocVienController : BaseHocVienController
    {
        E_ClassEntities1 db = new E_ClassEntities1();
        public ActionResult CourseOfUser(int id)
        {
            var Course = db.tbl_Deal.Where(x => x.UserID == id).OrderByDescending(x=>x.CreateDate).ToList();
            if (Course == null)
            {
                return View();
            }
            if (Course.Count() == 0)
            {
                ViewBag.Status = "Bạn chưa có khóa học nào";
            }
            return View(Course);
        }
        //Show infor of user
        //id is id of user
        [HttpGet]
        public ActionResult InforOfUser(int id)
        {
            var user = db.tbl_User.Find(id);
            if (user == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            return View(user);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult InforOfUser(tbl_User User)
        {

            db.Entry(User).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            ViewBag.Status = "Cập nhật thông tin thành công";
            return View();
        }
      
        public ActionResult WatchLession(int id, int studentId)//id is course id
        {
           
            //Kiểm tra xem học viên có khóa học chưa
            List<tbl_Deal> dealOfStudent = db.tbl_Deal.Where(x=>x.UserID== studentId).ToList();
            if(dealOfStudent == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            else
            {
                foreach(var item in dealOfStudent)
                {
                    if(item.CourseID==id)
                    {
                        var Lession = db.tbl_Lession.Where(x => x.CourseID == id).ToList();
                        var Course = db.tbl_Course.Find(id);
                        ViewBag.CourseName = Course.CourseName;
                        ViewBag.Teacher = Course.tbl_User.UserName;
                        ViewBag.TeacherID = Course.tbl_User.ID;
                        ViewBag.Users = Course.TotalStudents;
                        ViewBag.Censored = Course.censored;
                        ViewBag.CourseID = Course.ID;

                        //Tăng view khi xem khóa học
                        Course.TotalView = Course.TotalView + 1;
                        db.Entry(Course).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        return View(Lession);
                    }
                }
            }
            Response.StatusCode = 404;
            return null;
        }
        //id is id of Lession
        public ActionResult Lession(int id,int courseId, int studentId)
        {
           
            List<tbl_Deal> dealOfStudent = db.tbl_Deal.Where(x => x.UserID == studentId).ToList();
            if (dealOfStudent == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            else
            {
                foreach (var item in dealOfStudent)
                {
                    if (item.CourseID == courseId)
                    {
                        var Lession = db.tbl_Lession.Find(id);
                        ViewBag.Lession = Lession;
                        ViewBag.CourseID = Lession.tbl_Course.ID;

                        //Show list Lession
                        var ListLession = db.tbl_Lession.Where(x => x.CourseID == Lession.CourseID).ToList();
                        ViewBag.ListLession = ListLession;
                        return View();
                    }
                }
            }
            Response.StatusCode = 404;
            return null;
        }
        public ActionResult _PartialWatchLession(int id)
        {
            var Lession = db.tbl_Lession.Where(x => x.CourseID == id).ToList();
            return View(Lession);
        }
        public ActionResult CreateDeal(int UserID, int CourseID)
        {
            var User = db.tbl_User.Find(UserID);
            var Course = db.tbl_Course.Find(CourseID);
            if (User == null || Course == null)
            {
                Response.StatusCode = 404;
                return null;
            }

            if (Course.Pirce == null)
            {
                tbl_Deal Deal = new tbl_Deal();
                Deal.CreateDate = DateTime.Now;
                Deal.CourseID = CourseID;
                Deal.UserID = UserID;
                db.tbl_Deal.Add(Deal);
                db.SaveChanges();
            }
            return RedirectToAction("CourseOfUser", "HocVien",new {id= UserID});
        }
        [HttpPost]
        public ActionResult Comment(int studentId, string content,int courseId)
        {
            List<tbl_Deal> dealOfStudent = db.tbl_Deal.Where(x => x.UserID == studentId).ToList();
            if (dealOfStudent == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            else
            {
                foreach (var item in dealOfStudent)
                {
                    if (item.CourseID == courseId)
                    {
                        tbl_Comment comment = new tbl_Comment();
                        comment.Content = content;
                        comment.CourseID = courseId;
                        comment.CreateDay = DateTime.Now;
                        comment.UserID = studentId;
                        db.tbl_Comment.Add(comment);
                        db.SaveChanges();
                        return RedirectToAction("DetailCourse", "Home", new { id = courseId, userId = studentId });
                    }
                }
            }
            Response.StatusCode = 404;
            return null;
        }
    }
}