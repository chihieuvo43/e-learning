﻿using Common;
using CongNgheWeb_3_0.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CongNgheWeb_3_0.Controllers
{
    public class GiangVienController : BaseGiangVienController
    {
        E_ClassEntities1 db = new E_ClassEntities1();
        // GET: GiangVien
        public ActionResult Index()
        {
            
                return View();
           
        }
        [HttpGet]
        public ActionResult InforOfUser(int id)
        {
            var user = db.tbl_User.Find(id);
            if (user == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            return View(user);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult InforOfUser(tbl_User User)
        {
            db.Entry(User).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            ViewBag.Status = "Cập nhật thông tin thành công";
            return View();
        }
        [HttpGet]
        public ActionResult ChangePassword(int id)
        {
            var user = db.tbl_User.Find(id);
            if (user == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            return View(user);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ChangePassword(tbl_User User, string OldPassword, string NewPassword, string ConfirmNewPassword)
        {
            if (HashPassword.UnHash(OldPassword, User.PasswordUser) == false)
            {
                ViewBag.Status = "Mật khẩu không đúng - thử lại!";
                return View();
            }
            else if (NewPassword != ConfirmNewPassword)
            {
                ViewBag.Status = "Mật khẩu xác nhận không đúng - thử lại!";
                return View();
            }
            User.PasswordUser = HashPassword.Hash(NewPassword);
            db.Entry(User).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            Session.RemoveAll();
            return RedirectToAction("Index", "Home");
            //ViewBag.Status = "Cập nhật thông tin thành công";
            //return View();
        }
        //Course of teacher, id is id of teacher
        public ActionResult CourseOfTeacher(int id)
        {
            var Teacher = db.tbl_User.Find(id);
            if (Teacher == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            return View(Teacher);
        }
        //
        [HttpGet]
        public ActionResult CreateCourseByTeacher(int id)
        {
            var Teacher = db.tbl_User.Find(id);
            if (Teacher == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            ViewBag.MenuID = new SelectList(db.tbl_Menu.Where(x => x.ParentID != null), "ID", "MenuName");
            ViewBag.UserID = Teacher.ID;
            return View();
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateCourseByTeacher(tbl_Course Course, HttpPostedFileBase CourseImage)
        {
            ViewBag.MenuID = new SelectList(db.tbl_Menu.Where(x => x.ParentID != null).ToList(), "ID", "MenuName", Course.MenuID);
            if (CourseImage == null)
            {
                ViewBag.Status = "Chọn ảnh bìa cho khóa học";
                return View();
            }
            if (ModelState.IsValid)
            {
                var fileName = Path.GetFileName(CourseImage.FileName);
                var path = Path.Combine(Server.MapPath("~/Images/Courses"), fileName);
                if (System.IO.File.Exists(path))
                {
                    ViewBag.Status = "Ảnh bìa đã tồn tại";
                    return View();
                }
                else
                {
                    CourseImage.SaveAs(path);
                }
                Course.CourseImage = CourseImage.FileName;
                Course.censored = false;
                Course.TotalView = 0;
                Course.TotalStudents = 0;
                Course.CreateDate = DateTime.Now;
                Course.Deleted = false;
                Course.Sended = false;
                Course.Sended = false;

                Course.Discount = null;
                Course.Pirce = null;

                db.tbl_Course.Add(Course);
                db.SaveChanges();
                int CourseID = Course.ID;
                //return RedirectToAction("Index", "Course");
                return RedirectToAction("CreateLessionByTeacher", "GiangVien", new { @id = CourseID });
            }
            else
            {

                ViewBag.Status = "Không tạo được khóa học";
                return View();
            }
        }
        public ActionResult ShowLessionOfCourse(int id)
        {

            var Lession = db.tbl_Lession.Where(x => x.CourseID == id).OrderByDescending(x => x.LessionName).ToList();
            ViewBag.CountLession = Lession.Count();
            ViewBag.CourseName = db.tbl_Course.Single(x => x.ID == id).CourseName;
            ViewBag.CourseID = id;
            return View(Lession);
        }
        //Create lession by teacher
        [HttpGet]
        public ActionResult CreateLessionByTeacher(int id)
        {
            ViewBag.CourseID = db.tbl_Course.Single(x => x.ID == id).ID;
            ViewBag.CourseName = db.tbl_Course.Single(x => x.ID == id).CourseName;
            return View();
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateLessionByTeacher(int CourseID, tbl_Lession Lession)
        {
            ViewBag.CourseID = Lession.CourseID;
            ViewBag.CourseName = db.tbl_Course.Single(x => x.ID == CourseID).CourseName;

            if (ModelState.IsValid)
            {
                var LessionNameInData = db.tbl_Lession.SingleOrDefault(x => x.LessionName == Lession.LessionName);
                if (LessionNameInData != null)
                {
                    ViewBag.Status = "Tên bài học đã tồn tại";
                    return View();
                }
                //var LessionURLInData = db.tbl_Lession.SingleOrDefault(x => x.URLLession == Lession.URLLession);
                //if (LessionNameInData != null)
                //{
                //    ViewBag.Status = "Đường dẫn bài học đã tồn tại";
                //    return View();
                //}
                Lession.Deleted = false;
                Lession.CourseID = CourseID;
                db.tbl_Lession.Add(Lession);

                db.SaveChanges();
                return RedirectToAction("WatchLession", "GiangVien", new { @id = CourseID, teacherId=Lession.tbl_Course.UserID });
            }
            ViewBag.Status = "Không thêm được bài học";
            return View();
        }
        //Show Course of Teacher
        public ActionResult ShowCourseByTeacher(int id)
        {
            var Teacher = db.tbl_User.Find(id);
            if (Teacher == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            ViewBag.TeacherName = Teacher.UserName;
            var Course = db.tbl_Course.Where(x => x.UserID == id && x.Sended==false && x.censored==false).OrderByDescending(x => x.CreateDate).ToList();
            return View(Course);
        }
        //Show Course of teacher not censored- id is id of user
        public ActionResult ShowCourseNotCensored(int id)
        {
            var Teacher = db.tbl_User.Find(id);
            if (Teacher == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            ViewBag.TeacherName = Teacher.UserName;
            var Course = db.tbl_Course.Where(x => x.censored == false && x.UserID == id).OrderByDescending(x => x.Sended).ToList();
            return View(Course);
        }
        //Show Course of teacher censored
        public ActionResult ShowCourseCensored(int id)
        {
            var Teacher = db.tbl_User.Find(id);
            if (Teacher == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            ViewBag.TeacherName = Teacher.UserName;
            var Course = db.tbl_Course.Where(x => x.censored == true && x.UserID == id).OrderByDescending(x => x.CreateDate).ToList();
            return View(Course);
        }
        //Teacher watching detail of course- id of course
        public ActionResult ShowDetailCourse(int id)
        {
            var Course = db.tbl_Course.SingleOrDefault(x => x.ID == id);
            if (Course == null)
            {
                Response.StatusCode = 404;
                return null;
            }

            return View(Course);
        }
        //
        // GET: Course/Edit/5
        public ActionResult ShowEditCourse(int id)
        {
            //var Course = db.tbl_Course.SingleOrDefault(x => x.ID == id);
            var Course = db.tbl_Course.Find(id);
            if (Course == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            ViewBag.MenuID = new SelectList(db.tbl_Menu.Where(x => x.ParentID != null), "ID", "MenuName", Course.MenuID);
            return View(Course);
        }

        // POST: Course/Edit/5
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ShowEditCourse(tbl_Course Course, HttpPostedFileBase CourseImage, int Discount)
        {
            //ViewBag.CategoryID = new SelectList(db.tbl_Category.ToList(), "ID", "CategoryName", Course.CategoryID);
            if (Discount != 0)
            {
                Course.Discount = Discount;
            }
            if (CourseImage == null && Course.CourseImage == null)
            {
                ViewBag.Status = "Chọn ảnh bìa cho khóa học";
                return View();
            }
            if (ModelState.IsValid)
            {
                if (CourseImage != null)
                {
                    var fileName = Path.GetFileName(CourseImage.FileName);
                    var path = Path.Combine(Server.MapPath("~/Images/Courses"), fileName);
                    if (System.IO.File.Exists(path))
                    {
                        ViewBag.Status = "Ảnh bìa đã tồn tại";
                        return View();
                    }
                    else
                    {
                        CourseImage.SaveAs(path);
                    }
                    Course.CourseImage = CourseImage.FileName;
                }
            }
            db.Entry(Course).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            ViewBag.Status = "Sửa thành công";
            return RedirectToAction("ShowCourseByTeacher", "User", new { @id = Course.UserID });
        }
        //

        [HttpGet]
        public ActionResult SuaBaiHoc(int id)
        {
            //var Course = db.tbl_Course.SingleOrDefault(x => x.ID == id);
            var Lession = db.tbl_Lession.Find(id);
            if (Lession == null)
            {
                Response.StatusCode = 404;
                return null;
            }
           // ViewBag.MenuID = new SelectList(db.tbl_Menu.Where(x => x.ParentID != null), "ID", "MenuName", Lession.MenuID);
            return View(Lession);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SuaBaiHoc(tbl_Lession lession)
        {
            db.Entry(lession).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("WatchLession", "GiangVien", new { @id = lession.tbl_Course.ID, teacherId = lession.tbl_Course.UserID });
        }

        public ActionResult SendCourse(int id)
        {
            var Course = db.tbl_Course.Find(id);
            if (Course == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            ViewBag.MenuID = new SelectList(db.tbl_Menu.Where(x => x.ParentID != null), "ID", "MenuName", Course.MenuID);
            return View(Course);
        }
        //[HttpPost]
        //[ValidateInput(false)]
        public ActionResult ConnfirmSendCourse(int id)
        {
            tbl_Course Course = new tbl_Course();
            Course = db.tbl_Course.Find(id);
            if (Course == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            Course.Sended = true;

            db.Entry(Course).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("ShowDetailCourse", "GiangVien",new {id=Course.ID });
        }
        public ActionResult ShowDeleteCourse(int id)
        {

            var Course = db.tbl_Course.SingleOrDefault(x => x.ID == id);
            if (Course == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            //ViewBag.CategoryID = new SelectList(db.tbl_Category.ToList(), "ID", "CategoryName", Course.CategoryID);
            return View(Course);
        }
        public ActionResult ConfirmDelete(int id)
        {
            var CourseInData = db.tbl_Course.SingleOrDefault(x => x.ID == id);
            if (CourseInData == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            else
            {
                List<tbl_Lession> lessionInData = db.tbl_Lession.Where(x => x.CourseID == CourseInData.ID).ToList();
                if(lessionInData!=null)
                {
                    foreach(var item in lessionInData)
                    {
                        db.tbl_Lession.Remove(item);
                    }
                }
               
                db.tbl_Course.Remove(CourseInData);
                db.SaveChanges();
                return RedirectToAction("ShowCourseByTeacher", "GiangVien",new {id= CourseInData .tbl_User.ID});
            }
        }

        public ActionResult WatchLession(int id, int teacherId)//id is course id
        {

            //Kiểm tra xem học viên có khóa học chưa
            List<tbl_Course> courseOfTeacher = db.tbl_Course.Where(x => x.UserID == teacherId).ToList();
          
                foreach (var item in courseOfTeacher)
                {
                    if (item.ID == id)
                    {
                        var Lession = db.tbl_Lession.Where(x => x.CourseID == id).OrderBy(x=>x.LessionName).ToList();
                        var Course = db.tbl_Course.Find(id);
                        ViewBag.CourseName = Course.CourseName;
                        ViewBag.Teacher = Course.tbl_User.UserName;
                        ViewBag.TeacherID = Course.tbl_User.ID;
                        ViewBag.Users = Course.TotalStudents;
                        ViewBag.Censored = Course.censored;
                        ViewBag.CourseID = Course.ID;
                        ViewBag.Sended = Course.Sended;
                        return View(Lession);
                    }
                }
            Response.StatusCode = 404;
            return null;
        }
        //id is id of Lession
        public ActionResult Lession(int id, int courseId, int teacherId)
        {

            //Kiểm tra xem học viên có khóa học chưa
            List<tbl_Course> courseOfTeacher = db.tbl_Course.Where(x => x.UserID == teacherId).ToList();

            foreach (var item in courseOfTeacher)
            {
                if (item.ID == courseId)
                {
                    var Lession = db.tbl_Lession.Find(id);
                    ViewBag.Lession = Lession;
                    ViewBag.CourseID = Lession.tbl_Course.ID;

                    //Show list Lession
                    var ListLession = db.tbl_Lession.Where(x => x.CourseID == Lession.CourseID).ToList();
                    ViewBag.ListLession = ListLession;
                    return View(Lession);
                }
            }
            Response.StatusCode = 404;
            return null;
        }

        [HttpGet]
        public ActionResult DeleteLession(int id, int courseId, int teacherId)
        {
            //Kiểm tra xem học viên có khóa học chưa
            List<tbl_Course> courseOfTeacher = db.tbl_Course.Where(x => x.UserID == teacherId).ToList();

            foreach (var item in courseOfTeacher)
            {
                if (item.ID == courseId)
                {
                    var Lession = db.tbl_Lession.Find(id);
                    ViewBag.Lession = Lession;
                    ViewBag.CourseID = Lession.tbl_Course.ID;

                    //Show list Lession
                    var ListLession = db.tbl_Lession.Where(x => x.CourseID == courseId).ToList();
                    ViewBag.ListLession = ListLession;
                    return View(Lession);
                }
            }
            Response.StatusCode = 404;
            return null;
        }

        [HttpPost]
        public ActionResult ConfirmDeleteLession(int id, int courseId, int teacherId)
        {
            
            List<tbl_Course> courseOfTeacher = db.tbl_Course.Where(x => x.UserID == teacherId).ToList();

            foreach (var item in courseOfTeacher)
            {
                if (item.ID == courseId && item.censored==false)
                {
                    var Lession = db.tbl_Lession.SingleOrDefault(x => x.ID == id);
                    if (Lession == null)
                    {
                        Response.StatusCode = 404;
                        return null;
                    }
                    db.tbl_Lession.Remove(Lession);
                    db.SaveChanges();
                    return RedirectToAction("WatchLession", "GiangVien", new { id = Lession.CourseID, teacherId = teacherId });
                }
            }
            Response.StatusCode = 404;
            return null;
        }
    }

}