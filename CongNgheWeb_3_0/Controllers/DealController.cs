﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CongNgheWeb_3_0.Models;

namespace CongNgheWeb_3_0.Controllers
{
    public class DealController : BaseAdminController
    {
        E_ClassEntities1 db = new E_ClassEntities1();
        // GET: Deal
        public ActionResult Index()
        {
            return View();
        }

        // GET: Deal/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Deal/Create
    
        public ActionResult Create(int UserID,int CourseID)
        {
            var User = db.tbl_User.Find(UserID);
            var Course = db.tbl_Course.Find(CourseID);
            if(User == null || Course == null)
            {
                Response.StatusCode = 404;
                return null;
            }
           
            if (Course.Pirce==null)
            {
                tbl_Deal Deal = new tbl_Deal();
                Deal.CreateDate = DateTime.Now;
                Deal.CourseID = CourseID;
                Deal.UserID = UserID;
                db.tbl_Deal.Add(Deal);
                db.SaveChanges();
            }
            else
            {
                var Account = db.tbl_Account.Find(UserID);
                if (Account == null)
                {
                    ViewBag.Status = "Bạn chưa có số dư tài khoản!";
                    return RedirectToAction("DetailCourseDeal", "Home", new { @id = CourseID, @ms = ViewBag.Status });
                }
                if (Course.Discount==null)
                {

                    if(Account.Balance<Course.Pirce)
                    {
                        ViewBag.Status = "Tài khoản bạn không đủ đề mua khóa học!";
                        return RedirectToAction("DetailCourseDeal", "Home", new { @id = CourseID,@ms=ViewBag.Status});
                    }
                    else
                    {
                        tbl_Deal Deal = new tbl_Deal();
                        Deal.CreateDate = DateTime.Now;
                        Deal.CourseID = CourseID;
                        Deal.UserID = UserID;
                        db.tbl_Deal.Add(Deal);
                        db.SaveChanges();

                        //update Balance of user
                        Account.Balance -= Course.Pirce;
                        db.Entry(Account).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        ViewBag.Status = "Mua khóa học thành công!";
                        return RedirectToAction("DetailCourseDeal", "Home", new { @id = CourseID, @ms = ViewBag.Status });
                    }
                }
                //Discount != null
            }
            return View();
        }

        // POST: Deal/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Deal/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Deal/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Deal/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Deal/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
