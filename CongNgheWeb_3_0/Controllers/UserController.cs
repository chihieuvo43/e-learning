﻿using System;
using Common;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CongNgheWeb_3_0.Models;
using PagedList;
using PagedList.Mvc;
using System.IO;
using System.Configuration;

namespace CongNgheWeb_3_0.Controllers
{
    public class UserController : BaseAdminController
    {
        E_ClassEntities1 db = new E_ClassEntities1()
;        // GET: User
        public ActionResult Index()
        {
            return View();
        }
      
        public static string GenerateRandomPassword(int length)
        {
            string allowedLetterChars = "abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ";
            string allowedNumberChars = "0123456789";
            char[] chars = new char[length];
            Random rd = new Random();
            bool useLetter = true;
            for (int i = 0; i < length; i++)
            {
                if (useLetter)
                {
                    chars[i] = allowedLetterChars[rd.Next(0, allowedLetterChars.Length)];
                    useLetter = false;
                }
                else
                {
                    chars[i] = allowedNumberChars[rd.Next(0, allowedNumberChars.Length)];
                    useLetter = true;
                }
            }
            return new string(chars);
        }

        //Get Create teacher

        public ActionResult CreateTeacher()
        {
            return View();
        }


        //Post Create teacher
        //[HttpPost,ActionName("CreateTeacher")]
        //[ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult CreateUserDontSendMail(tbl_User user)
        {
            user.PasswordUser = "123456";
            //string CreateRandomPassword = GenerateRandomPassword(6);
            var EmailInData = db.tbl_User.SingleOrDefault(x => x.Email == user.Email);
            if(EmailInData != null)
            {
                ViewBag.Status = "Email đã tồn tại. Xin hãy nhập Email khác!";
                return View("CreateTeacher");
            }
            else 
            {
                try
                {
                    tbl_User User = new tbl_User();
                    User.Clock = false;
                    User.CreateDate = DateTime.Now;
                    User.DescriptionUser = null;
                    User.Email = user.Email;
                    User.LoginName = user.Email;
                    User.Notificationed = false;
                    User.NumberPhone = user.NumberPhone;
                    User.PasswordUser = HashPassword.Hash(user.PasswordUser);
                    User.Position = 3;
                    User.SendMail = true;
                    User.UserImage = "no-image.jpg";
                    User.UserName = user.UserName;

                    db.tbl_User.Add(User);
                    db.SaveChanges();
                    return RedirectToAction("ListTeacher", "Admin");
                }
                catch
                {
                    ViewBag.Status = "Không tạo được tài khoản";
                }
                   

            }
          
            ViewBag.Status = "Không tạo được tài khoản";
            return View("CreateTeacher");
        }

        //Post dont send mail
        //[HttpPost]
        public ActionResult CreateUserAndSendMail(string UserName, string Email)
        {
            string CreateRandomPassword = GenerateRandomPassword(6);
            var EmailInData = db.tbl_User.SingleOrDefault(x => x.Email == Email);
            if (EmailInData != null)
            {
                ViewBag.Status = "Email đã tồn tại. Xin hãy nhập Email khác!";
                //return View("CreateTeacher", "User");
                return View("CreateTeacher");
            }
            else
            {
                tbl_User User = new tbl_User();
                if (ModelState.IsValid)
                {
                    User.Clock = false;
                    User.CreateDate = DateTime.Now;
                    User.DescriptionUser = null;
                    User.Email = Email;
                    User.LoginName = Email;
                    User.NumberPhone = null;
                    User.PasswordUser = HashPassword.Hash(CreateRandomPassword);
                    User.UserImage = null;
                    User.UserName = UserName;
                    User.Position = 3;
                    User.SendMail = true;
                    User.Notificationed = true;
                    try
                    {
                        //send mail to teacher
                        string content = System.IO.File.ReadAllText(Server.MapPath("~/Content/Admin/MailSendToTeacher.html"));

                        content= content.Replace("{{CustomName}}", User.UserName);
                        content = content.Replace("{{LoginName}}", Email);
                        content = content.Replace("{{Password}}", CreateRandomPassword);
                        var toMail = ConfigurationManager.AppSettings["ToEmailAddress"].ToString();
                        new MailHelper().SendMail(toMail, "Tài khoản vào EClass", content);
                    }
                    catch (Exception)
                    {
                        ViewBag.Status = "Không gửi được mail nên không tạo được tài khoản";
                        return View("CreateTeacher");
                    }


                    db.tbl_User.Add(User);
                    db.SaveChanges();
                    return RedirectToAction("ListTeacher", "Admin");
                }

            }

            ViewBag.Status = "Không tạo được tài khoản";
            return View("CreateTeacher");
        }

        //Detail teacher
        public ActionResult DetailTeacher(int id)
        {
            var Teacher = db.tbl_User.Find(id);
            if(Teacher==null)
            {
                return null;
            }
            return View(Teacher);
        }
        //Clock and Unclock User
        public ActionResult ClockAndUnClock(int id)
        {
            var User = db.tbl_User.SingleOrDefault(x => x.ID == id);
            if(User.Clock==true)
            {
               
                db.Entry(User).State = System.Data.Entity.EntityState.Modified;
                User.Clock = false;
                db.SaveChanges();
                return RedirectToAction("ListTeacher", "Admin");
            }
            else
            {
               
                db.Entry(User).State = System.Data.Entity.EntityState.Modified;
                User.Clock = true;
                db.SaveChanges();
                return RedirectToAction("ListTeacher", "Admin");
            }
        }



        //Remove User

        [HttpGet]
        public ActionResult Delete(int? id)
        {
            var User = db.tbl_User.SingleOrDefault(x => x.ID == id);
            if (User == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            return View(User);
        }

        [HttpPost,ActionName("Delete")]
        public ActionResult DeleteConfirm(int id)
        {
            var User = db.tbl_User.SingleOrDefault(x => x.ID == id);
            if(User==null)
            {
                Response.StatusCode = 404;
                return null;
            }
            db.tbl_User.Remove(User);
            db.SaveChanges();

            return RedirectToAction("ListTeacher", "Admin");
        }

        [HttpGet]
        public ActionResult CreateStudent()
        {
            return View();
        }
        //Student
        //[ActionName("CreateStudent")]
        [HttpPost]
        public ActionResult CreateStudent(tbl_User student)
        {
            var mailInData = db.tbl_User.SingleOrDefault(x => x.Email ==student.Email);
            if (mailInData != null)
            {
                ViewBag.ConfirmMail = "Email này đã đăng ký !";
                return View();
            }
            //if (password != confirmpassword)
            //{
            //    ViewBag.ConfirmPassword = "Mật khẩu xác nhận không đúng !";
            //    return View();
            //}
            tbl_User Student = new tbl_User();
            Student.Clock = false;
            Student.CreateDate = DateTime.Now;
            Student.DescriptionUser = null;
            Student.Email = student.Email;
            Student.LoginName = student.Email;
            Student.NumberPhone = student.NumberPhone;
            Student.PasswordUser = HashPassword.Hash(student.PasswordUser);
            Student.Position = 4;
            Student.SendMail = true;
            Student.UserImage = "no-image.jpg";
            Student.UserName = student.UserName;

            db.tbl_User.Add(Student);
            db.SaveChanges();
            ViewBag.Status = "Tạo tài khoản thành công";
            return View();


        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }


        public ActionResult ListStudent(int? page)
        {
            int pageSize = 5;

            int pageNumber = (page ?? 1);
            return View(db.tbl_User.Where(x => x.Position == 4).ToList().ToPagedList(pageNumber,pageSize));
        }

        public ActionResult DetailsStudent(int id)
        {
            var Student = db.tbl_User.Find(id);
            if(Student==null)
            {
                Response.StatusCode = 404;
                return null;
            }
            ViewBag.TongKhoaHoc = db.tbl_Deal.Where(x => x.UserID == id).Count();
            return View(Student);
        }
        public ActionResult CourseOfStudent(int id)
        {
            var Student = db.tbl_User.Find(id);
            if (Student == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            List<tbl_Course> course = db.tbl_Course.Where(x => x.Sended == true).ToList();
            List<tbl_Course> courseOfStudent = new List<tbl_Course>();
            List<tbl_Deal> dealOfStudent = db.tbl_Deal.Where(x => x.UserID == id).ToList();

            foreach(var item in course)
            {
                foreach(var itemDeal in dealOfStudent)
                {
                    if(item.ID== itemDeal.CourseID)
                    {
                        courseOfStudent.Add(item);
                    }
                }
            }
            ViewBag.Student = Student;
            return View(courseOfStudent);
        }
        [HttpGet]
        public ActionResult DeleteStudent(int id)
        {
            var Student = db.tbl_User.Find(id);
            if (Student == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            return View(Student);
        }

        [HttpPost,ActionName("DeleteStudent")]
        public ActionResult ConfirmDeleteStudent(int id)
        {
            var Student = db.tbl_User.Find(id);
            db.tbl_User.Remove(Student);
            db.SaveChanges();
            return RedirectToAction("ListStudent", "User");
        }

        //Show infor of user
        //id is id of user
        [HttpGet]
        public ActionResult InforOfUser(int id)
        {
            var user = db.tbl_User.Find(id);
            if(user==null)
            {
                Response.StatusCode = 404;
                return null;
            }
            return View(user);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult InforOfUser(tbl_User User)
        {
           
            db.Entry(User).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            ViewBag.Status = "Cập nhật thông tin thành công";
            return View();
        }
        //change of user
        //id is id of user
        [HttpGet]
        public ActionResult ChangePassword(int id)
        {
            var user = db.tbl_User.Find(id);
            if (user == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            return View(user);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ChangePassword(tbl_User User,string OldPassword, string NewPassword, string ConfirmNewPassword)
        {
            if(HashPassword.UnHash(OldPassword,User.PasswordUser)==false)
            {
                ViewBag.Status = "Mật khẩu không đúng - thử lại!";
                return View();
            }
            else if(NewPassword != ConfirmNewPassword)
            {
                ViewBag.Status = "Mật khẩu xác nhận không đúng - thử lại!";
                return View();
            }
            User.PasswordUser = HashPassword.Hash(NewPassword);
            db.Entry(User).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            Session.RemoveAll();
            return RedirectToAction("Index", "Home");
            //ViewBag.Status = "Cập nhật thông tin thành công";
            //return View();
        }

        //Course of user
        public ActionResult CourseOfUser(int id)
        {
            var Course = db.tbl_Deal.Where(x => x.UserID == id).ToList();
            if(Course==null)
            {
                return View();
            }
            if (Course.Count() == 0)
            {
                ViewBag.Status = "Bạn chưa có khóa học nào";
            }
            return View(Course);
        }
        //Watch lession
        //id is id of course
        public ActionResult WatchLession(int id)
        {
            var Lession = db.tbl_Lession.Where(x => x.CourseID == id).ToList();
            var Course = db.tbl_Course.Find(id);
            ViewBag.CourseName = Course.CourseName;
            ViewBag.Teacher = Course.tbl_User.UserName;
            ViewBag.TeacherID = Course.tbl_User.ID;
            ViewBag.Users = Course.TotalStudents;
            ViewBag.Censored = Course.censored;
            ViewBag.CourseID = Course.ID;
            Course.TotalView = Course.TotalView + 1;
            db.Entry(Course).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return View(Lession);
        }
        //id is id of Lession
        public ActionResult Lession(int id)
        {
            var Lession = db.tbl_Lession.Find(id);
            ViewBag.Lession = Lession;
            ViewBag.CourseID = Lession.tbl_Course.ID;

            //Show list Lession
            var ListLession = db.tbl_Lession.Where(x => x.CourseID == Lession.CourseID).ToList();
            ViewBag.ListLession = ListLession;
            return View();
        }
        public ActionResult _PartialWatchLession(int id)
        {
            var Lession = db.tbl_Lession.Where(x => x.CourseID == id).ToList();
            return View(Lession);
        }
        //Infor of teacher
        public ActionResult InforTeacher(int id)
        {
            var Teacher = db.tbl_User.Find(id);
            ViewBag.Teacher = Teacher;

            var Course = db.tbl_Course.Where(x => x.UserID == id && x.censored==true).Take(4).ToList();
            ViewBag.Course = Course;

            return View();
        }
        //Course of teacher, id is id of teacher
        public ActionResult CourseOfTeacher(int id)
        {
            var Teacher = db.tbl_User.Find(id);
            if(Teacher==null)
            {
                Response.StatusCode = 404;
                return null;
            }
            return View(Teacher);
        }
        //
        [HttpGet]
        public ActionResult CreateCourseByTeacher(int id)
        {
            var Teacher = db.tbl_User.Find(id);
            if (Teacher == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            ViewBag.MenuID = new SelectList(db.tbl_Menu.Where(x => x.ParentID != null), "ID", "MenuName");
            ViewBag.UserID = Teacher.ID;
            return View();
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateCourseByTeacher(tbl_Course Course, HttpPostedFileBase CourseImage)
        {
            ViewBag.MenuID = new SelectList(db.tbl_Menu.Where(x => x.ParentID != null).ToList(), "ID", "MenuName", Course.MenuID);
            if (CourseImage == null)
            {
                ViewBag.Status = "Chọn ảnh bìa cho khóa học";
                return View();
            }
            if (ModelState.IsValid)
            {
                var fileName = Path.GetFileName(CourseImage.FileName);
                var path = Path.Combine(Server.MapPath("~/Images/Courses"), fileName);
                if (System.IO.File.Exists(path))
                {
                    ViewBag.Status = "Ảnh bìa đã tồn tại";
                    return View();
                }
                else
                {
                    CourseImage.SaveAs(path);
                }
                Course.CourseImage = CourseImage.FileName;
                Course.censored = false;
                Course.TotalView = 0;
                Course.TotalStudents = 0;
                Course.CreateDate = DateTime.Now;
                Course.Deleted = false;
                db.tbl_Course.Add(Course);
                db.SaveChanges();
                int CourseID = Course.ID;
                //return RedirectToAction("Index", "Course");
                return RedirectToAction("CreateLessionByTeacher", "User",new {@id= CourseID });
            }
            else
            {

                ViewBag.Status = "Không tạo được khóa học";
                return View();
            }
        }
        public ActionResult ShowLessionOfCourse(int id)
        {
          
            var Lession = db.tbl_Lession.Where(x => x.CourseID == id).OrderByDescending(x => x.LessionName).ToList();
            ViewBag.CountLession = Lession.Count();
            ViewBag.CourseName = db.tbl_Course.Single(x => x.ID == id).CourseName;
            ViewBag.CourseID = id;
            return View(Lession);
        }
        //Create lession by teacher
        [HttpGet]
        public ActionResult CreateLessionByTeacher(int id)
        {
            ViewBag.CourseID = db.tbl_Course.Single(x => x.ID == id).ID;
            ViewBag.CourseName = db.tbl_Course.Single(x => x.ID == id).CourseName;
            return View();
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateLessionByTeacher(int CourseID, tbl_Lession Lession)
        {
            ViewBag.CourseID = Lession.CourseID;
            ViewBag.CourseName = db.tbl_Course.Single(x => x.ID == CourseID).CourseName;

            if (ModelState.IsValid)
            {
                var LessionNameInData = db.tbl_Lession.SingleOrDefault(x => x.LessionName == Lession.LessionName);
                if (LessionNameInData != null)
                {
                    ViewBag.Status = "Tên bài học đã tồn tại";
                    return View();
                }
                //var LessionURLInData = db.tbl_Lession.SingleOrDefault(x => x.URLLession == Lession.URLLession);
                //if (LessionNameInData != null)
                //{
                //    ViewBag.Status = "Đường dẫn bài học đã tồn tại";
                //    return View();
                //}
                Lession.Deleted = false;
                Lession.CourseID = CourseID;
                db.tbl_Lession.Add(Lession);

                db.SaveChanges();
                return RedirectToAction("WatchLession", "User", new { @id = CourseID });
            }
            ViewBag.Status = "Không thêm được bài học";
            return View();
        }
        //Show Course of Teacher
        public ActionResult ShowCourseByTeacher(int id)
        {
            var Teacher = db.tbl_User.Find(id);
            if (Teacher == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            ViewBag.TeacherName = Teacher.UserName;
            var Course = db.tbl_Course.Where(x => x.UserID == id).OrderByDescending(x=>x.CreateDate).ToList();
            return View(Course);
        }
        //Show Course of teacher not censored- id is id of user
        public ActionResult ShowCourseNotCensored(int id)
        {
            var Teacher = db.tbl_User.Find(id);
            if (Teacher == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            ViewBag.TeacherName = Teacher.UserName;
            var Course = db.tbl_Course.Where(x => x.censored == false && x.UserID==id).OrderByDescending(x => x.CreateDate).ToList();
            return View(Course);
        }
        //Show Course of teacher censored
        public ActionResult ShowCourseCensored(int id)
        {
            var Teacher = db.tbl_User.Find(id);
            if (Teacher == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            ViewBag.TeacherName = Teacher.UserName;
            var Course = db.tbl_Course.Where(x => x.censored == true && x.UserID == id).OrderByDescending(x => x.CreateDate).ToList();
            return View(Course);
        }
        //Teacher watching detail of course- id of course
        public ActionResult ShowDetailCourse(int id)
        {
            var Course = db.tbl_Course.SingleOrDefault(x => x.ID == id);
            if (Course == null)
            {
                Response.StatusCode = 404;
                return null;
            }

            return View(Course);
        }
        //
        // GET: Course/Edit/5
        public ActionResult ShowEditCourse(int id)
        {
            //var Course = db.tbl_Course.SingleOrDefault(x => x.ID == id);
            var Course = db.tbl_Course.Find(id);
            if (Course == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            ViewBag.MenuID = new SelectList(db.tbl_Menu.Where(x => x.ParentID != null), "ID", "MenuName", Course.MenuID);
            return View(Course);
        }

        // POST: Course/Edit/5
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ShowEditCourse(tbl_Course Course, HttpPostedFileBase CourseImage, int Discount)
        {
            //ViewBag.CategoryID = new SelectList(db.tbl_Category.ToList(), "ID", "CategoryName", Course.CategoryID);
            if (Discount != 0)
            {
                Course.Discount = Discount;
            }
            if (CourseImage == null && Course.CourseImage == null)
            {
                ViewBag.Status = "Chọn ảnh bìa cho khóa học";
                return View();
            }
            if (ModelState.IsValid)
            {
                if (CourseImage != null)
                {
                    var fileName = Path.GetFileName(CourseImage.FileName);
                    var path = Path.Combine(Server.MapPath("~/Images/Courses"), fileName);
                    if (System.IO.File.Exists(path))
                    {
                        ViewBag.Status = "Ảnh bìa đã tồn tại";
                        return View();
                    }
                    else
                    {
                        CourseImage.SaveAs(path);
                    }
                    Course.CourseImage = CourseImage.FileName;
                }
            }
            db.Entry(Course).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            ViewBag.Status = "Sửa thành công";
            return RedirectToAction("ShowCourseByTeacher","User",new {@id=Course.UserID});
        }
        //
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateLessionInLession(string LessionName,string URL,string Time,int CourseID, int LessionID)
        {
            try
            {
                if (LessionName == null)
                {
                    ViewBag.Status = "Không được bỏ trống tên khóa học";
                    return RedirectToAction("Lession", "User", new { @id = LessionID });
                }
                if (URL == null)
                {
                    ViewBag.Status = "Không được bỏ trống tên đường dẫn";
                    return RedirectToAction("Lession", "User", new { @id = LessionID });
                }
                if (Time == null)
                {
                    ViewBag.Status = "Không được bỏ trống thời gian";
                    return RedirectToAction("Lession", "User", new { @id = LessionID });
                }
                tbl_Lession Lession = new tbl_Lession();
                Lession.CourseID = CourseID;
                Lession.Deleted = false;
                Lession.LessionName = LessionName;
                Lession.TotalTime = Convert.ToInt32(Time);
                Lession.URLLession = URL;
                db.tbl_Lession.Add(Lession);
                db.SaveChanges();
                int NewLessionID = Lession.ID;
                return RedirectToAction("Lession", "User", new { @id = NewLessionID });

            }
            catch
            {
                return RedirectToAction("Lession", "User", new { @id = LessionID });
            }
         }
       
    }
}