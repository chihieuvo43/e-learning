﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CongNgheWeb_3_0.Models
{
    [MetadataTypeAttribute(typeof(MenuMetadata))]
    public partial class tbl_Menu
    {
        internal sealed class MenuMetadata
        {
            public int ID { get; set; }

            [Display(Name ="Tên danh mục")]
            [Required(ErrorMessage ="Không được bỏ trống tên danh mục")]
            public string MenuName { get; set; }

           
            public Nullable<int> ParentID { get; set; }

            [Display(Name = "Icon")]
            public string Icon { get; set; }
        }
    }
}