﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CongNgheWeb_3_0.Models
{
    [MetadataTypeAttribute(typeof(UserMetadata))]
    public partial class tbl_User
    {
        internal sealed class UserMetadata
        {
            public int ID { get; set; }

            [Display(Name ="Tên người dùng")]
            [Required(ErrorMessage ="Không được bỏ trống tên người dùng")]
            public string UserName { get; set; }

            [Display(Name = "Hình ảnh")]
            public string UserImage { get; set; }

            [Display(Name = "Tên đăng nhập")]
            public string LoginName { get; set; }

            [Display(Name = "Mật khẩu")]
            public string PasswordUser { get; set; }

            [Display(Name = "Thông tin cá nhân")]
            public string DescriptionUser { get; set; }

            [Display(Name = "Ngày tạo")]
            [DataType(DataType.Date)]
            public Nullable<System.DateTime> CreateDate { get; set; }

            [Display(Name = "Email")]
            [Required(ErrorMessage = "Không được bỏ trống email")]
            public string Email { get; set; }

            [Display(Name = "Khóa")]
            public Nullable<bool> Clock { get; set; }

            [Display(Name = "Quyền")]
            public Nullable<int> Position { get; set; }

            [Display(Name = "Đã gửi mail")]
            public Nullable<bool> SendMail { get; set; }

            [Display(Name = "Đã thông báo")]
            public Nullable<bool> Notificationed { get; set; }

            [Display(Name = "Đã xóa")]
            public Nullable<bool> Deleted { get; set; }

            [Display(Name = "Số điện thoại")]
            [MaxLength(10,ErrorMessage ="Số điện thoại là 10 số")]
            [DataType(DataType.PhoneNumber)]
            public string NumberPhone { get; set; }
        }
    }
}