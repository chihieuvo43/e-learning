//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CongNgheWeb_3_0.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_Menu
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_Menu()
        {
            this.tbl_Course = new HashSet<tbl_Course>();
        }
    
        public int ID { get; set; }
        public string MenuName { get; set; }
        public Nullable<int> ParentID { get; set; }
        public string Icon { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_Course> tbl_Course { get; set; }
    }
}
